#!/usr/bin/env bash
apt-get update --fix-missing;
apt-get install -f -y build-essential autoconf automake make libssl-dev libffi-dev git curl wget nano zip tree iftop iotop realpath;
apt-get install software-properties-common;
apt-add-repository -y ppa:ansible/ansible;
apt-get update --fix-missing;
apt-get install -y ansible;